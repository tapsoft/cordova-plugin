
// This is for importing the cordova package explicitly
var cordova = require('cordova');
var    exec    = require('cordova/exec');
 var channel = require('cordova/channel');

var BTXPLUGIN = function(){

    // This is used in defining the 
    this.channels = {
        onConnect : channel.create('onConnect'),
        onReceive : channel.create('onReceive'),
        onStopBroadcast : channel.create('onStopBroadcast'),
        onDisconnect : channel.create('onDisconnect')
    };

    // for (var key in this.channels) {
    //     this.channels[key].onHasSubscribersChange = BTXPLUGIN.onHasSubscribersChange;
    // }
};

BTXPLUGIN.prototype.addEventListener = function (eventname,f) {

    if (eventname in btxPlugin.channels) {
        btxPlugin.channels[eventname].subscribe(f);
    }
};

BTXPLUGIN.prototype.removeEventListener = function(eventname, f) {
    if (eventname in btxPlugin.channels) {
        btxPlugin.channels[eventname].unsubscribe(f);
    }
};

// This is called to tell the app to be ready to accept/send connection
BTXPLUGIN.prototype.broadcast = function(name,successCallback, errorCallback){
    // This called to start the listening service

    var successCall = function(data){
        callBackHandler(data,successCallback);
    };

    exec(successCall,
            errorCallback, 
            "BTX",
            "broadcast",
            [name]);
};


// This is called to connect to device whose address is specified
BTXPLUGIN.prototype.connect = function(address, successCallback, errorCallback){

    
    var successCall = function(data){
        callBackHandler(data,successCallback);
    };

    exec(successCall,
            errorCallback, 
            "BTX",
            "connect",
            [address]);
};

BTXPLUGIN.prototype.enableLocationService = function(successCallback, errorCallback){

    
    var successCall = function(data){
        callBackHandler(data,successCallback);
    };

    exec(successCall,
            errorCallback, 
            "BTX",
            "enableLocationService",
            []);
};

// This is called to send a message to a device
BTXPLUGIN.prototype.send = function(message, successCallback, errorCallback){

    
    var successCall = function(data){

        // alert(btxPlugin.channels.onReceive.numHandlers + "  numHandlers ");
        callBackHandler(data,successCallback);

    };

    exec(successCall,
            errorCallback, 
            "BTX",
            "send",
            [message]);
};

// This is called to stop the device from listening to any connection
BTXPLUGIN.prototype.disconnect = function(successCallback, errorCallback){

    var successCall = function(data){
        callBackHandler(data,successCallback);
    };

    exec(successCall,
            errorCallback, 
            "BTX",
            "disconnect",
            []);
};

// This is called to stop the device from listening to any connection
BTXPLUGIN.prototype.stopBroadcast = function(successCallback, errorCallback){

    var successCall = function(data){
        callBackHandler(data,successCallback);
    }

    exec(successCall,
            errorCallback, 
            "BTX",
            "stopBroadcast",
            []);
};

var btxPlugin = new BTXPLUGIN();

function callBackHandler(data, successCallback){
    if(data.operation === "receiving"){

        if(btxPlugin.channels.onReceive.numHandlers > 0){
            btxPlugin.channels['onReceive'].fire(data.message);
        }
    }else if(data.operation === "connection"){

        if(btxPlugin.channels.onConnect.numHandlers > 0){

            btxPlugin.channels['onConnect'].fire(data.status);

        }
        
    }else if(data.operation === "stopBroadcast"){

        if(btxPlugin.channels.onStopBroadcast.numHandlers > 0){

            btxPlugin.channels['onStopBroadcast'].fire(data.status);

        }
        
    }else if(data.operation === "disconnect"){

        if(btxPlugin.channels.onDisconnect.numHandlers > 0){

            btxPlugin.channels['onDisconnect'].fire(data.status);

        }
        
    }else{
        successCallback(data);
    }
}

module.exports = btxPlugin;
