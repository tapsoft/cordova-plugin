
#import <Foundation/Foundation.h>

extern NSString *const isConnected;
extern NSString *const isPeerConnected;
extern NSString *const isListening;
// extern NSString *const shouldBroadcast;

@interface Globals : NSObject

@end