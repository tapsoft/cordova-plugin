/**
* Author : Emmanuel Adeyemi
* Date : 29/05/2016
* 
*/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Cordova/CDV.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BTX : CDVPlugin <CBCentralManagerDelegate, CBPeripheralDelegate,CBPeripheralManagerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *peripheral;
@property (strong, nonatomic) CBCharacteristic *characteristics;
@property (strong, nonatomic) NSMutableData *data;
@property (strong, nonatomic) CBMutableService * customService;
@property (strong, nonatomic) CBMutableCharacteristic *customCharacteristics;
@property (strong, nonatomic) CBPeripheralManager *manager;

@property (strong, nonatomic) NSData  *dataToSend;
@property (nonatomic, readwrite) NSInteger sendDataIndex;

// These are the main plugin methods
-(void) connect : (CDVInvokedUrlCommand*)command ;
-(void) broadcast : (CDVInvokedUrlCommand*)command;
-(void) send : (CDVInvokedUrlCommand*)command ;
-(void) stopBroadcast : (CDVInvokedUrlCommand*)command ;
-(void) disconnect : (CDVInvokedUrlCommand*)command;

@end 