
/**
* Author : Emmanuel Adeyemi
* Date : 29/05/2016
**/
#import "BTX.h"
#import <Cordova/CDV.h>


static NSString* const serviceUUID = @"C5AD10CF-4195-48C6-AE0E-6032B70929AF";
static NSString* const characteristicUUID = @"84D64504-4BCE-4B7F-A83E-D45C041D79A3";
bool canWrite = false;
static NSString* serviceName = @"NairaBox";
static NSString* peerName = @"";
bool canBroadcast = false;
bool shouldBroadcast = false;
bool isBroadcasting = false;

#define NOTIFY_MTU  20

@implementation BTX{
}

/**
*  This is for connecting to a device
*/
-(void) connect : (CDVInvokedUrlCommand*)command {
	// check if bluetooth is enabled
	// if not enabled throw error with message : "BLUETOOTH NOT ENABLED"
	
	// This called to initialize the callback
	[UIApplication sharedApplication].idleTimerDisabled = YES;

	CDVPluginResult* pluginResult = nil;

	NSString* address = [command.arguments objectAtIndex:0];

	[[NSUserDefaults standardUserDefaults] setObject:command.callbackId forKey:@"COMMANDID"];

	if([address isEqualToString:@""]){

		[self sendNotification:NO dataAsString:@"HOST_NAME_UNAVAILABLE" error:YES];
		return;
	}
    
    // if([pName isEqualToString:@""]){
        
    //     [self sendNotification:@"HOST_NAME_UNAVAILABLE" wait:NO error:YES];
    //     return ;
    // }
    
    peerName = [address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    canWrite = false;
    isBroadcasting = false;
    
    if(self.manager){
        [self.manager stopAdvertising];
        shouldBroadcast = false;
    }
    
    [self.commandDelegate runInBackground:^{
    	NSLog(@"Starting connection with %@",peerName);

    	self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
	}];
	// if(canBroadcast == false){

	// 	pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"BLUETOOTH_OFF"];
 //    	[self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

 //    	return ;
	// }
}

/**
* This call to broadcast it's bluetooth service
*/
-(void) broadcast : (CDVInvokedUrlCommand*)command{
	// check if bluetooth is enabled
	// if not enabled throw error with message : "BLUETOOTH NOT ENABLED"
	// CDVPluginResult* pluginResult = nil;
	[UIApplication sharedApplication].idleTimerDisabled = YES;
	
	NSString* address = [command.arguments objectAtIndex:0];

	[[NSUserDefaults standardUserDefaults] setObject:command.callbackId forKey:@"COMMANDID"];

	if([address isEqualToString:@""]){

		// [self sendNotification:@"HOST_NAME_UNAVAILABLE" wait:NO error:YES];
		[self sendNotification:NO dataAsString:@"HOST_NAME_UNAVAILABLE" error:YES];
		return;
	}


    address = [address stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    shouldBroadcast = true;
    
    serviceName = address;
    // This is used initialising the peripheral manager to start broadcasting
    [self.commandDelegate runInBackground:^{
	    self.manager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
	}];
	
}

-(void) send : (CDVInvokedUrlCommand*)command{

	CDVPluginResult* pluginResult = nil;

	NSString* message = [command.arguments objectAtIndex:0];

	if(isBroadcasting == false){

        if(!canWrite){
            NSLog(@"Device is not connected");
            return ;
        }
        
        [self write:message];
    }
    else
        [self writePeripheral:message];

}

-(void) stopBroadcast : (CDVInvokedUrlCommand*)command {

	shouldBroadcast = false;
	isBroadcasting = false;
    [self.manager stopAdvertising];
}

-(void) disconnect : (CDVInvokedUrlCommand*)command{

	if(self.centralManager && self.peripheral){
		[self.commandDelegate runInBackground:^{
			[self.centralManager cancelPeripheralConnection:self.peripheral];
		}];
	}
}

-(void) write : (NSString *) message{
    
    NSData *dataToWrite = [message dataUsingEncoding:NSUTF8StringEncoding];
    
    [self.peripheral writeValue:dataToWrite forCharacteristic:self.characteristics type:CBCharacteristicWriteWithResponse];
    
}

/** This section is for peripherals */
-(void)centralManagerDidUpdateState:(CBCentralManager *)central{
    
    switch (central.state) {
        case CBCentralManagerStatePoweredOff:
    		// [self sendNotification: wait:NO error:YES];
        [self sendNotification:NO dataAsString:@"BLUETOOTH_OFF" error:YES];
            break;
        case CBCentralManagerStatePoweredOn:{
	        	NSLog(@"about to scan for device");
	        	[self.commandDelegate runInBackground:^{
	        		[self sendNotification:YES dataAsString:@"CONNECTION_STARTED" error:NO];
		            [self.centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:serviceUUID]] options:@{CBCentralManagerScanOptionAllowDuplicatesKey : @YES}];
		        }];
	        }
            break;
            
        default:
            break;
    }
}

-(void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI{
    
    NSString* name = [advertisementData objectForKey:@"kCBAdvDataLocalName"];

    if(self.peripheral != peripheral){
        
        NSLog(@"Device Name is %@ : peername : %@",name, peerName);
        
        if(![[name lowercaseString] isEqualToString:[peerName lowercaseString]]){
            return ;
        }
        
        
        self.peripheral = peripheral;
        
        [self.centralManager stopScan];
        [self.centralManager connectPeripheral:self.peripheral options:nil];
    }
}

-(void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    
    NSLog(@"Connection to peripheral is established");
    
    [self.data setLength:0];
    // This is used in setting the peripheral's delegate
    [self.peripheral setDelegate:self];
    
    // This is called to discover the service based on the UUID provided
    [self.peripheral discoverServices:@[[CBUUID UUIDWithString:serviceUUID]]];
}

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    
    if(error){
        NSLog(@"Error discovering service %@", [error localizedDescription]);
        // [self sendNotification:@"HOST_UNAVAILABLE" wait:NO error:YES];
        [self sendNotification:NO dataAsString:@"HOST_UNAVAILABLE" error:YES];
        return;
    }
    
    long size = [[peripheral services] count];
    
    NSLog(@"Hello: %ld: ", size);
    
    for(CBService *service in peripheral.services){
        
        if([service.UUID isEqual:[CBUUID UUIDWithString:serviceUUID]]){
            
            NSString *serviceName = [service.peripheral name];
            
            NSLog(@"Peripheral Nama: %@",serviceName);
            [self.peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:characteristicUUID]] forService:service];
        }
    }
}

-(void) peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    
    if(error){
        NSLog(@"Error discovering service %@", [error localizedDescription]);
        return ;
    }
    
    for(CBCharacteristic *characteristic in service.characteristics){
        
        if([characteristic.UUID isEqual:[CBUUID UUIDWithString:characteristicUUID]]){
            
            self.characteristics = characteristic;
            // This is used to subscribe to the characteristic
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
		    // [dict setValue:@"initialization" forKey:@"operation"];
		    [dict setValue:@"connection" forKey:@"operation"];
		    [dict setValue:@"HOST_CONNECTION_OK" forKey:@"status"];
		    [self sendNotification:YES dataAsDictionary:dict error:NO];

            canWrite = true;
        }
    }
    
}

-(void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    if(error){
        NSLog(@"Error reading from character : %@", error.localizedDescription);
        return;
    }
    
    // self.data = [characteristic.value mutableCopy];
    
    // if(self.data != nil){
        
    //     NSString *response = [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding];
        
    //     NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	   //  // [dict setValue:@"initialization" forKey:@"operation"];
	   //  [dict setValue:@"receiving" forKey:@"operation"];
	   //  [dict setValue:response forKey:@"message"];
	   //  // [self sendNotification:dict wait:YES error:NO];
	   //  [self sendNotification:YES dataAsDictionary:dict error:NO];
        
    //    NSLog(@"Received %@", response);
    // }

    NSMutableData *dData = [characteristic.value mutableCopy];
    
    if(dData != nil){
        
        NSString *response = [[NSString alloc] initWithData:dData encoding:NSUTF8StringEncoding];
        
        if ([response isEqualToString:@"EOM"]) {
            
            // Cancel our subscription to the characteristic
//            [peripheral setNotifyValue:NO forCharacteristic:characteristic];
//            
//            // and disconnect from the peripehral
//            [self.centralManager cancelPeripheralConnection:peripheral];
            response = [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding];
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setValue:@"receiving" forKey:@"operation"];
            [dict setValue:response forKey:@"message"];
            // [self sendNotification:dict wait:YES error:NO];
            [self sendNotification:YES dataAsDictionary:dict error:NO];
            
            self.data = [[NSMutableData alloc] init];
            
            return;
        }
        
        // Otherwise, just add the data on to what we already have
        [self.data appendData:characteristic.value];
        
//        NSLog(@"Received %@", response);
    }

    
}

-(void) centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    
    if(error){
        NSLog(@"Error during connection %@", [error localizedDescription]);
        // [self sendNotification:@"HOST_UNAVAILABLE" wait:NO error:YES];
        [self sendNotification:NO dataAsString:@"HOST_UNAVAILABLE" error:YES];

        //[PaypadFacade sendNotification:dict wait:NO error:NO];
    }
}

/** This is for peripheral **/
-(void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
    
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOff:
            NSLog(@"Peripheral is off");
            // [self sendNotification:@"BLUETOOTH_OFF" wait:NO error:YES];
            [self sendNotification:NO dataAsString:@"BLUETOOTH_OFF" error:YES];
            break;
        case CBPeripheralManagerStatePoweredOn:
            NSLog(@"peripheral is on");
            
            if([self.manager isAdvertising])
                [self.manager stopAdvertising];
            
            [self setUpService];
            break;
        default:
            break;
    }
}

-(void) setUpService{
    
    CBUUID *cbuuid = [CBUUID UUIDWithString:characteristicUUID];
    
    self.customCharacteristics = [[CBMutableCharacteristic alloc] initWithType:cbuuid properties:CBCharacteristicPropertyNotify|CBCharacteristicPropertyWrite value:nil permissions:CBAttributePermissionsReadable|CBAttributePermissionsWriteable];
    
    CBUUID *serviceuuid = [CBUUID UUIDWithString:serviceUUID];
    
    self.customService = [[CBMutableService alloc] initWithType:serviceuuid primary:YES];
    
    // This add characteristics to service
    [self.customService setCharacteristics:@[self.customCharacteristics]];
    
    isBroadcasting = true;
    
    // This adds service to manager
    [self.manager addService:self.customService];
    
    //    [self.manager startA]
    
}

-(void) peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error{
    
    if(error == nil){
        [self.manager startAdvertising:@{ CBAdvertisementDataLocalNameKey : serviceName, CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:serviceUUID]]}];
    }else{
        NSLog(@"Unable to start advertising service %@: ", [error localizedDescription]);
    }
}

-(void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central 
		didSubscribeToCharacteristic:(CBCharacteristic *)characteristic{
    
  // This is called when a central subscribe to broadcaster characteristics
    
    NSLog(@"Peripheral has subscribe to device");
    
    // [self showAlert:@"Attention" message:@"One device is connected"];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    // [dict setValue:@"initialization" forKey:@"operation"];
    [dict setValue:@"connection" forKey:@"operation"];
    [dict setValue:@"HOST_CONNECTION_OK" forKey:@"status"];
    [self sendNotification:YES dataAsDictionary:dict error:NO];
    // This is called when advertising it on
    [self.manager stopAdvertising];
    
}

- (void)sendData
{
    // First up, check if we're meant to be sending an EOM
    static BOOL sendingEOM = NO;
//    int sendDataIndex;
    
    if (sendingEOM) {
        
        // send it
        BOOL didSend = [self.manager updateValue:[@"EOM" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:self.customCharacteristics onSubscribedCentrals:nil];
        
        // Did it send?
        if (didSend) {
            
            // It did, so mark it as sent
            sendingEOM = NO;
            
            NSLog(@"Sent: EOM");
        }
        
        // It didn't send, so we'll exit and wait for peripheralManagerIsReadyToUpdateSubscribers to call sendData again
        return;
    }
    
    // We're not sending an EOM, so we're sending data
    
    // Is there any left to send?
    
    if (self.sendDataIndex >= self.dataToSend.length) {
        
        // No data left.  Do nothing
        return;
    }
    
    // There's data left, so send until the callback fails, or we're done.
    
    BOOL didSend = YES;
    
    while (didSend) {
        
        // Make the next chunk
        
        // Work out how big it should be
        NSInteger amountToSend = self.dataToSend.length - self.sendDataIndex;
        
        // Can't be longer than 20 bytes
        if (amountToSend > NOTIFY_MTU) amountToSend = NOTIFY_MTU;
        
        // Copy out the data we want
        NSData *chunk = [NSData dataWithBytes:self.dataToSend.bytes+ self.sendDataIndex length:amountToSend];
        
        // Send it
        didSend = [self.manager updateValue:chunk forCharacteristic:self.customCharacteristics onSubscribedCentrals:nil];
        
        // If it didn't work, drop out and wait for the callback
        if (!didSend) {
            return;
        }
        
        NSString *stringFromData = [[NSString alloc] initWithData:chunk encoding:NSUTF8StringEncoding];
        NSLog(@"Sent: %@", stringFromData);
        
        // It did send, so update our index
        self.sendDataIndex += amountToSend;
        
        // Was it the last one?
        if (self.sendDataIndex >= self.dataToSend.length) {
            
            // It was - send an EOM
            
            // Set this so if the send fails, we'll send it next time
            sendingEOM = YES;
            
            // Send it
            BOOL eomSent = [self.manager updateValue:[@"EOM" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:self.customCharacteristics onSubscribedCentrals:nil];
            
            if (eomSent) {
                // It sent, we're all done
                sendingEOM = NO;
                
                NSLog(@"Sent: EOM");
            }
            
            return;
        }
    }
}

-(void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central 
		didUnsubscribeFromCharacteristic:(CBCharacteristic *)characteristic{
    // This is called when a central disconnect from the peripheral
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    // [dict setValue:@"initialization" forKey:@"operation"];
    [dict setValue:@"connection" forKey:@"operation"];
    [dict setValue:@"HOST_DISCONNECTION_OK" forKey:@"status"];
    [self sendNotification:YES dataAsDictionary:dict error:NO];

    [self.manager removeService:self.customService];
    self.characteristics = nil;
    self.customService = nil;
    self.manager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];

    if(shouldBroadcast == true){
        // This is called again to s
        [self setUpService];
    }
}

/**
** This is called when broadcast is started
*/
- (void) peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error{
    // This is called when the peripheral start advertising
    
    if(error){
        NSLog(@"Unable to start advertising %@", error.localizedDescription);
        [self sendNotification:NO dataAsString:@"BROADCAST_FAILED" error:YES];
        return ;
    }

    [self sendNotification:YES dataAsString:@"BROADCAST_STARTED" error:NO];
}

-(void) writePeripheral : (NSString *) message{
    
    // [self.manager updateValue:[message dataUsingEncoding:NSUTF8StringEncoding] 
    // 	forCharacteristic:self.customCharacteristics onSubscribedCentrals:nil];

    self.dataToSend =[message dataUsingEncoding:NSUTF8StringEncoding];
    
    // Reset the index
    self.sendDataIndex = 0;
    
    [self sendData];

}


-(void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray<CBATTRequest *> *)requests{
    
    CBATTRequest *request = nil;
    
    for(CBATTRequest *req in requests){
        
        if([req.characteristic.UUID isEqual:[CBUUID UUIDWithString:characteristicUUID]]){
            
            request = req;
            break;
        }
    }
    
    if(request == nil)
        return;
    
    
    NSLog(@"Data received is %@",[[NSString alloc] initWithData:request.value encoding:NSUTF8StringEncoding]);
    
    NSString *msg = [NSString stringWithFormat:@"%@",[[NSString alloc] initWithData:request.value encoding:NSUTF8StringEncoding]];
    
    // UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Receiving Status" message:msg delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
    
    // [alertView show];

    // jObject.put("operation","receiving");
    // jObject.put("message", new String(message));
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    // [dict setValue:@"initialization" forKey:@"operation"];
    [dict setValue:@"receiving" forKey:@"operation"];
    [dict setValue:msg forKey:@"message"];
    [self sendNotification:YES dataAsDictionary:dict error:NO];

    self.customCharacteristics.value = request.value;
    
    [self.manager respondToRequest:request withResult:CBATTErrorSuccess];
    
}

-(void) peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request{
    
    if(![request.characteristic.UUID isEqual:[CBUUID UUIDWithString:characteristicUUID]]){
        
        return;
    }
    
    if(request.offset > self.customCharacteristics.value.length){
        
        [peripheral respondToRequest:request withResult:CBATTErrorInvalidOffset];
        return;
    }
    
    //    NSString *message = [[NSString alloc] initWithData:self.customCharacteristics.value encoding:NSUTF8StringEncoding];
    
    request.value = [[self.customCharacteristics value] subdataWithRange:NSMakeRange(request.offset, self.customCharacteristics.value.length - request.offset)];
    
    [peripheral respondToRequest:request withResult:CBATTErrorSuccess];
    //NSLog(@"Read %@:", message);
}

/* This section handle notification */

-(void)sendNotification:(BOOL) wait dataAsString:(NSString *) message
                    error:(BOOL) error{
    
    
    NSString* commandId = [[NSUserDefaults standardUserDefaults] objectForKey:@"COMMANDID"];

    CDVPluginResult* pluginResult = nil;
    
    if(error == YES){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:message];
    }else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    }
    
    [pluginResult setKeepCallbackAsBool:wait];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:commandId];
    
} 

-(void)sendNotification:(BOOL) wait dataAsDictionary:(NSDictionary *) dict
                    error:(BOOL) error{
    
    // dispatch_async(dispatch_get_main_queue(),^{
        NSString* commandId = [[NSUserDefaults standardUserDefaults] objectForKey:@"COMMANDID"];
    
        CDVPluginResult* pluginResult = nil;
        
        if(error == YES){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dict];
        }else{
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
        }
        

        [pluginResult setKeepCallbackAsBool:wait];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:commandId];

        // return ;
    // });
    
} 

/** This callback comes in when the PeripheralManager is ready to send the next chunk of data.
 *  This is to ensure that packets will arrive in the order they are sent
 */
- (void)peripheralManagerIsReadyToUpdateSubscribers:(CBPeripheralManager *)peripheral
{
    // Start sending again
    [self sendData];
}

@end