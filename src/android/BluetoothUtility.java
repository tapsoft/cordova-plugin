package com.swipemax.btx;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import org.apache.cordova.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;
import org.json.JSONObject;
import java.util.Arrays;
import android.content.Intent;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;

/**
 * Created by adeyemi on 5/24/16.
 */
public class BluetoothUtility {

    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
    public static final int STATE_DISCONNECTED = 4;  // now discconnected from a remote device
    // This is the UUID which the device will use
    // private final UUID SPP_UUID = UUID.fromString("db87c0d0-afac-11de-8a39-0803200c9a89");
    private final UUID SPP_UUID = UUID.fromString("C5AD10CF-4195-48C6-AE0E-6032B70929AF");
    private final UUID characteristicUUID = UUID.fromString("84D64504-4BCE-4B7F-A83E-D45C041D79A3");
    static BluetoothAdapter bluetoothAdapter;
    private static final String TAG = "BluetoothUtility";
    private Context context;

    private static final String NAME_CONNECTION = "NairaBox";

    private AcceptThread mInsecureAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState;

    // This section is for handling BLE
    private BluetoothGatt bluetoothGatt;
    private boolean isBLEConnected = false;
    private BluetoothGattCharacteristic bluetoothGattCharacteristic;

    private String recievedData = "";

    // private boolean isBLEConnected = false;

    public BluetoothUtility(Context context){
        this.context = context;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device The BluetoothDevice to connect
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    public synchronized void connect(BluetoothDevice device, boolean secure) {
        Log.d(TAG, "connect to: " + device);

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device, secure);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Set the current state of the chat connection
     *
     * @param state An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;

        // Give the new state to the Handler so the UI Activity can update
        //mHandler.obtainMessage(Constants.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
        String message = "";
        JSONObject jObject = new JSONObject();
        PluginResult r ;

        switch (state) {
            case STATE_DISCONNECTED:

                try{
                    jObject.put("operation","disconnect");
                    jObject.put("status","HOST_DISCONNECTION_SUCCESSFUL");
                }catch(Exception ex){}

                r = new PluginResult(PluginResult.Status.OK, jObject);
                r.setKeepCallback(false);
                BTX.callbackContext.sendPluginResult(r);

                //BTX.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "HOST_DISCONNECTION_OK"));
                message = "disconnected";
                Globals.isPeerConnected = false;
                break;
            case STATE_CONNECTED:

                try{

                    jObject.put("operation", "connection");
                    jObject.put("status", "HOST_CONNECTION_OK");

                    r = new PluginResult(PluginResult.Status.OK, jObject);
                    r.setKeepCallback(true);
                    BTX.callbackContext.sendPluginResult(r);
                }catch(Exception e){

                }
                Globals.isPeerConnected = true;
                message = "connected";
                break;
            case STATE_CONNECTING:
                message = "connecting";
                Globals.isPeerConnected = false;
                break;
            case STATE_LISTEN:
            case STATE_NONE:
                Globals.isPeerConnected = false;
                message = "not connected";
                break;
        }

//        Toast.makeText(context, "STATE: "+message , Toast.LENGTH_SHORT).show();

        // showMessage("STATE: "+message);
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice
            device, final String socketType) {
        Log.d(TAG, "connected, Socket Type:" + socketType);

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
            // showMessage("stopping connect thread");
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            // showMessage("stopping connected thread");
            mConnectedThread = null;
        }

        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
            // showMessage("stopping accept thread");
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket, socketType);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
//        Message msg = mHandler.obtainMessage(Constants.MESSAGE_DEVICE_NAME);
//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.DEVICE_NAME, device.getName());
//        msg.setData(bundle);
//        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;
        private String mSocketType;

        public AcceptThread(boolean secure) {
            BluetoothServerSocket tmp = null;
            mSocketType = secure ? "Secure" : "Insecure";

            // Create a new listening server socket
            try {
                    tmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(
                            NAME_CONNECTION, SPP_UUID);

                    PluginResult r = new PluginResult(PluginResult.Status.OK, "BROADCAST_STARTED");
                    r.setKeepCallback(true);

                    BTX.callbackContext.sendPluginResult(r);

            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed", e);
                BTX.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "BROADCAST_FAILED"));
            }
            mmServerSocket = tmp;
        }

        public void run() {
            Log.d(TAG, "Socket Type: " + mSocketType +
                    "BEGIN mAcceptThread" + this);
            setName("AcceptThread" + mSocketType);

            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    Globals.isListening = true;
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    Globals.isListening = false;
                    Log.e(TAG, "Socket Type: " + mSocketType + "accept() failed", e);
                    break;
                }catch(Exception e){
                    Globals.isListening = false;
                    Log.e(TAG, "Socket Type: " + mSocketType + "accept() failed", e);
                    break;
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized (BluetoothUtility.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                // Situation normal. Start the connected thread.
                                connected(socket, socket.getRemoteDevice(),
                                        mSocketType);
                                break;
                            case STATE_NONE:
                            case STATE_CONNECTED:
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    Log.e(TAG, "Could not close unwanted socket", e);
                                }
                                break;
                        }
                    }
                }
            }
            Log.i(TAG, "END mAcceptThread, socket Type: " + mSocketType);

        }

        public void cancel() {
            Log.d(TAG, "Socket Type" + mSocketType + "cancel " + this);
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Socket Type" + mSocketType + "close() of server failed", e);
            }
        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private String mSocketType;

        public ConnectThread(BluetoothDevice device, boolean secure) {
            mmDevice = device;
            BluetoothSocket tmp = null;
            mSocketType = secure ? "Secure" : "Insecure";

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = device.createInsecureRfcommSocketToServiceRecord(SPP_UUID);
            } catch (IOException e) {
                Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectThread SocketType:" + mSocketType);
            setName("ConnectThread" + mSocketType);

            // Always cancel discovery because it will slow down a connection
            bluetoothAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket.connect();
            } catch (IOException e) {
                // Close the socket
                try {
                    mmSocket.close();

                    if(Globals.hasBLE)
                        connectBLE(mmDevice);
                    else
                        BTX.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "CONNECTION_FAILED"));
                        // connectionFailed();

                } catch (IOException e2) {
                    Log.e(TAG, "unable to close() " + mSocketType +
                            " socket during connection failure", e2);

                    // showMessage("Error connecting: "+e2.getMessage());
                }
                // connectionFailed();

                
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothUtility.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice, mSocketType);
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect " + mSocketType + " socket failed", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private InputStream mmInStream;
        private OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket, String socketType) {
            Log.d(TAG, "create ConnectedThread: " + socketType);
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

            Globals.isPeerConnected = true;
            Globals.isListening = true;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");
            byte[] buffer = new byte[1024];
            int bytes;

            // Keep listening to the InputStream while connected
            while (mState == STATE_CONNECTED || Globals.isListening == true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);

                    // Send the obtained bytes to the UI Activity
//                    mHandler.obtainMessage(Constants.MESSAGE_READ, bytes, -1, buffer)
//                            .sendToTarget();
                    

                    String msg = (new String(buffer))+"";

                    // showMessage("READ: "+ msg);

                    JSONObject jObject = new JSONObject();
                    try{

                        byte[] message = Arrays.copyOfRange(buffer,0,bytes);
                        jObject.put("operation","receiving");
                        jObject.put("message", new String(message));
                        

                    }catch(Exception ex){

                    }

                    PluginResult r = new PluginResult(PluginResult.Status.OK, jObject);
                    r.setKeepCallback(true);
                    BTX.callbackContext.sendPluginResult(r);

                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);
                   connectionLost();
                    // Start the service over to restart listening mode
                    // BluetoothUtility.this.start();
                    Globals.isPeerConnected = false;

                    // showMessage("Connection is lost");
                    break;
                }catch(Exception ex){
                    connectionLost();
                    // Start the service over to restart listening mode
                    // BluetoothUtility.this.start();
                    Globals.isPeerConnected = false;

                    showMessage("Connection is lost");
                    break;
                }
            }
        }


        public void connectionLost(){

            Globals.isPeerConnected = false;

            try{
                if(mmOutStream != null){
                    mmOutStream.close();
                    mmOutStream = null;
                }
                
                if(mmInStream != null){
                    mmInStream.close();
                    mmInStream = null;
                }
                
            }catch(IOException ex){
            }catch(Exception ex){

            }
        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer The bytes to write
         */
        public void write(byte[] buffer) {
            try {

                // showMessage("WRITE: "+ new String(buffer));

                System.out.println("Hello");

                mmOutStream.write(buffer);

                // Share the sent message back to the UI Activity
//                mHandler.obtainMessage(Constants.MESSAGE_WRITE, -1, -1, buffer)
//                        .sendToTarget();

                // showMessage("WRITE: "+new String(buffer));

            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
                showMessage("Error during writing "+e.getMessage());
            }
        }

        public void cleanUp(){

            try {
                if (mmInStream != null) {
                    mmInStream.close();
                    mmInStream = null;
                }

                if (mmOutStream != null) {
                    mmOutStream.close();
                    mmOutStream = null;
                }

            }catch(IOException ex){

            }catch(Exception ex){}

        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }


    }


    /**
    * This called to start scanning
    */
    public void startScan(){

        // If we're already discovering, stop it
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter
        bluetoothAdapter.startDiscovery();
    }

    public void showMessage(final String message){

        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        });

    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start() {
        Log.d(TAG, "start");

       // // Cancel any thread attempting to make a connection
       //  if (mConnectThread != null) {
       //      mConnectThread.cancel();
       //      mConnectThread = null;
       //  }

       //  // Cancel any thread currently running a connection
       //  if (mConnectedThread != null) {
       //      mConnectedThread.cancel();
       //      mConnectedThread = null;
       //  }

        setState(STATE_LISTEN);

        if (mInsecureAcceptThread == null) {
            mInsecureAcceptThread = new AcceptThread(false);
            mInsecureAcceptThread.start();
        }
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
        // Create temporary object
        if(Globals.usingBLE) {
            sendMessage(out);

        }else {
            ConnectedThread r;
            // Synchronize a copy of the ConnectedThread
            synchronized (this) {
                if (mState != STATE_CONNECTED) return;
                r = mConnectedThread;
            }
            // Perform the write unsynchronized
            r.write(out);
        }
    }

    @TargetApi(value = 19)
    private void sendMessage(byte[] msg){
        if(bluetoothGattCharacteristic != null){

            bluetoothGattCharacteristic.setValue(msg);

            bluetoothGatt.writeCharacteristic(bluetoothGattCharacteristic);
        }
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        // Send a failure message back to the Activity
//        Message msg = mHandler.obtainMessage(Constants.MESSAGE_TOAST);
//        Bundle bundle = new Bundle();
//        bundle.putString(Constants.TOAST, "Unable to connect device");
//        msg.setData(bundle);
//        mHandler.sendMessage(msg);

        // showMessage("Unable to connect");

        BTX.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "CONNECTION_FAILED"));

        // Start the service over to restart listening mode
        BluetoothUtility.this.start();
    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }

    public synchronized void stop() {
        Log.d(TAG, "stop");

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if (mConnectedThread != null) {
            mConnectedThread.cleanUp();
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread.cancel();
            mInsecureAcceptThread = null;
        }
        setState(STATE_NONE);
    }

    public synchronized void stopBroadcast() {
        Log.d(TAG, "stop");

        try{
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }

            if (mInsecureAcceptThread != null) {
                mInsecureAcceptThread.cancel();
                mInsecureAcceptThread = null;
            }
        }catch(Exception ex){
            
        }

        Globals.isListening = false;
        // setState(STATE_NONE);
    }

    public synchronized void stopAndBroadcast() {
        Log.d(TAG, "stop");

       try{
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }

            if (mConnectedThread != null) {
                mConnectedThread.cancel();
                mConnectedThread = null;
            }

            if (mInsecureAcceptThread != null) {
                mInsecureAcceptThread.cancel();
                mInsecureAcceptThread = null;
            }
            setState(STATE_NONE);

            
        }catch(Exception ex){

        }

        // ensureDiscoverable(Globals.deviceConnectName);
        Globals.isListening = false;
        start();
    }

    public synchronized void disconnectPeer(){

        if (mConnectedThread != null) {
            mConnectedThread.cleanUp();
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        if(bluetoothGatt != null){
            bluetoothGatt.disconnect();
        }

        setState(STATE_DISCONNECTED);
    }

    // This is called to enable discovery
    public void ensureDiscoverable(String name){

        // This is to set the name that will be broadcasted
        bluetoothAdapter.setName(name);
        if (bluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0); // 0 makes permanently visible
            ((Activity)context).startActivityForResult(discoverableIntent, Globals.VISIBILITY_CALLBACK);
        }else{
            start();
        }
    }

    public void connectBLE(BluetoothDevice device){
//
        device.connectGatt(context, false, new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {

//                    Toast.makeText(context, "device connected", Toast.LENGTH_LONG).show();
                    System.out.println("Service is discovered");
                    //Discover services
                    gatt.discoverServices();


                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {


                    Globals.usingBLE = true;

                    try {
                        gatt.close();
                    }catch(Exception ex){

                    }

                    //Handle a disconnect event
                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            
                            try{

                                String message = "";
                                JSONObject jObject = new JSONObject();
                                PluginResult r ;
                                jObject.put("operation","disconnect");
                                jObject.put("status","HOST_DISCONNECTION_SUCCESSFUL");

                                r = new PluginResult(PluginResult.Status.OK, jObject);
                                r.setKeepCallback(false);
                                BTX.callbackContext.sendPluginResult(r);
                            }catch(Exception ex){

                            }

                        }
                    });

                }
            }

            @Override
            public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                System.out.println("There is a write function called");
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {

                if (status == BluetoothGatt.GATT_SUCCESS) {
                    BluetoothGattService service = gatt.getService(SPP_UUID);
                    if (service == null) {
//                        Toast.makeText(context, "Device is not found", Toast.LENGTH_LONG).show();
                        System.out.println("Device is not found");
                    }else{
                        System.out.println("Device is found");

                        bluetoothGatt = gatt;

                        BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUUID);
                        gatt.setCharacteristicNotification(characteristic,true);

                        if(characteristic != null){
                            System.out.println("Characteristics is available");

                            BluetoothGattDescriptor descriptor = null;

                            bluetoothGattCharacteristic = characteristic;
                            descriptor = characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
                            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);

                            gatt.writeDescriptor(descriptor);

                            isBLEConnected = true;

                            Globals.usingBLE = true;
                            try{

                                JSONObject jObject = new JSONObject();
                                jObject.put("operation", "connection");
                                jObject.put("status", "HOST_CONNECTION_OK");

                                PluginResult r = new PluginResult(PluginResult.Status.OK, jObject);
                                r.setKeepCallback(true);
                                BTX.callbackContext.sendPluginResult(r);
                            }catch(Exception e){

                            }
                        }else{
                            // System.out.println("Characteristics is not available");
                            isBLEConnected = false;
                            Globals.usingBLE = false;
                        }
                    }


                } else {

                    Globals.usingBLE = false;
                }

            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

                if(status == BluetoothGatt.GATT_SUCCESS){

                   byte[] data =  characteristic.getValue();

                    String dataString = new String(data);

                    System.out.println("Received "+dataString);
                }
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

                byte[] message = characteristic.getValue();

                String messageRead = "";

                // if(message != null && message.length > 0){
                //     System.out.println("Data read is "+new String(message));

                //      messageRead = new String(message);
                // }

                if(message != null && message.length > 0){

                    String received = new String(message);

                    if(received.equalsIgnoreCase("EOM")){

                        System.out.println("Data read is "+recievedData);

                        JSONObject jObject = new JSONObject();

                        try{

                            jObject.put("operation","receiving");
                            jObject.put("message", recievedData);
                            

                        }catch(Exception ex){

                        }

                        PluginResult r = new PluginResult(PluginResult.Status.OK, jObject);
                        r.setKeepCallback(true);
                        BTX.callbackContext.sendPluginResult(r);

                         recievedData = "";


                        return;
                    }

                    recievedData += received;
//                    System.out.println("Data read is "+new String(message));
                }



                // System.out.println("Characteristics has changed ");

            }

//            onch
        });
    }
}
