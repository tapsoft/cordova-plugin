
package com.swipemax.btx;

public class Globals{
	
	public static boolean isConnected = false;
	public static boolean isPeerConnected = false;
	public static boolean isListening = false;
	public static boolean shouldBroadcast = false;
	public static String deviceConnectName = null;
	public final static int VISIBILITY_CALLBACK = 1000;
	public static String INITIAL_DEVICE_NAME = "";
	public static boolean IS_BLUETOOTH_PERMITTED = false;
	public static boolean IS_PERMITTED = false;
	public static boolean hasBLE = false;
    public static boolean usingBLE = false;
    public static String deviceBroadCastName = "";
}