package com.swipemax.btx;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.app.Activity;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.Toast;
import org.json.JSONObject;
import android.os.Build;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.content.Context;
import android.Manifest;
import android.provider.Settings;

public class BTX extends CordovaPlugin{

	private BluetoothAdapter bluetoothAdapter;

	private BluetoothUtility bluetoothUtility;
	private String deviceAddress = null, deviceConnectName;
	public static CallbackContext callbackContext;
	private final int REQUEST_ENABLE_BT = 1;
    private final int REQUEST_ENABLE_BT_AND_CONNECT = 2;
    private final int REQUEST_ENABLE_BT_AND_BROADCAST = 3;
    private final int BLUETOOTH_PERMISSION_REQUEST = 20;
    private final int LOCATION_PERMISSION_REQUEST = 10;

    JSONArray args;

	private Activity activity;


	private BroadcastReceiver mStateReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            // showMessage("Name: "+nName);

//            Log.i(TAG, action);
            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
	            //Do something if connected
	            //Toast.makeText(context, "BT Connected", Toast.LENGTH_SHORT).show();
                // Globals.isConnected = true;

                // showMessage("host connected");
                Globals.isPeerConnected = true;
	        }
	        else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
	            //Do something if disconnected
	            // Toast.makeText(context, "BT Disconnected", Toast.LENGTH_SHORT).show();
                Globals.isPeerConnected = false;
                // showMessage("Peer Disconnected");

                if(Globals.shouldBroadcast == true){
                    bluetoothUtility.stopAndBroadcast();
                }
                else
                    bluetoothUtility.stop();
                
	        }else if( BluetoothDevice.ACTION_FOUND.equals(action)){

                String nName = device.getName();

                // showMessage("Name: "+nName);
                // This compares the name with that provided
                if(nName != null)
                    if(nName.equalsIgnoreCase(Globals.deviceConnectName)){
                        deviceAddress = bluetoothAdapter.getAddress();

                        if (bluetoothAdapter.isDiscovering()) {
                            bluetoothAdapter.cancelDiscovery();
                        }

                        // showMessage("about to connect");

                        bluetoothUtility.connect(device, false);
                    }

            }else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equalsIgnoreCase(action)){
                deviceAddress = null;
//                Globals.deviceConnectName = null;
            }else if( BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equalsIgnoreCase(action)){

                if(deviceAddress == null){
                    // Toast.makeText(cordova.getActivity(), "Unable to connect to device", Toast.LENGTH_LONG).show();
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "HOST_UNAVAILABLE"));
                }
            }
        }
    };

	@Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        activity = cordova.getActivity();

        args = data;

        BTX.callbackContext = callbackContext;

        if(action.equalsIgnoreCase("broadcast")){

            // if(Globals.IS_BLUETOOTH_PERMITTED == false){
            //     callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "BLUETOOTH_NOT_PERMITTED"));
            //     return true;
            // }

            PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
            r.setKeepCallback(true);
            callbackContext.sendPluginResult(r);

            // showMessage("Listening "+Globals.isListening);

            String broadcastName = args.getString(0);

            if(broadcastName == null || broadcastName.trim().isEmpty()){
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "NAME_NOT_PROVIDED"));
                return true;
            }

            Globals.shouldBroadcast = true;

        	if(Globals.isListening == false && Globals.isPeerConnected == false){

                r = new PluginResult(PluginResult.Status.OK, "BROADCAST_STARTING");
                r.setKeepCallback(true);
                callbackContext.sendPluginResult(r);

                if(bluetoothAdapter.isEnabled() == false){

                    Globals.deviceBroadCastName = broadcastName;
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    cordova.getActivity().startActivityForResult(enableIntent, REQUEST_ENABLE_BT_AND_BROADCAST);
                }else{
                    Globals.INITIAL_DEVICE_NAME = bluetoothAdapter.getName();
                    bluetoothUtility.ensureDiscoverable(broadcastName);
                }

                // bluetoothUtility.start();
            }else if(Globals.isPeerConnected){
                r = new PluginResult(PluginResult.Status.OK, "HOST_CONNECTED_AS_PEER");
                r.setKeepCallback(true);
                callbackContext.sendPluginResult(r);
            }else{

                r = new PluginResult(PluginResult.Status.OK, "ALREADY_BROADCASTING");
                r.setKeepCallback(true);
                callbackContext.sendPluginResult(r);
            }

        	return true;
        }else if(action.equalsIgnoreCase("connect")){

            PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
            r.setKeepCallback(true);
            callbackContext.sendPluginResult(r);

            // if(Globals.IS_BLUETOOTH_PERMITTED == false){
            //     callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "BLUETOOTH_NOT_PERMITTED"));
            //     return true;
            // }

            String buildRelease = Build.VERSION.RELEASE.substring(0, 2);

            double releaseNo = Double.parseDouble(buildRelease);

            String d = args.getString(0);

            Globals.deviceConnectName = d;
            
            PackageManager pm = cordova.getActivity().getPackageManager();
            boolean hasBLE = pm.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);

            Globals.hasBLE  = hasBLE;

            if((releaseNo >= 6.0 || Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) && hasBLE){                

                if(d == null){
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "HOST_NOT_PROVIDED"));
                    return true;
                }

                if((cordova.hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION) && cordova.hasPermission(Manifest.permission.BLUETOOTH)) && cordova.hasPermission(Manifest.permission.BLUETOOTH_ADMIN)){
                    Globals.IS_BLUETOOTH_PERMITTED = true;

                    // showMessage("Has permission");

                    if(isLocationEnabled(cordova.getActivity()) == false){

                        // showMessage("enable location service");

                        Globals.IS_PERMITTED = false;
                        r = new PluginResult(PluginResult.Status.OK, "ENABLE_LOCATION_SERVICE");
                        r.setKeepCallback(true);
                        callbackContext.sendPluginResult(r);

                    }else{
                        Globals.IS_PERMITTED = true;

                        Globals.IS_BLUETOOTH_PERMITTED = true;
                        

                        enableBluetoothAndConnect();
                    }   
                }else{
                    cordova.requestPermissions(this,BLUETOOTH_PERMISSION_REQUEST,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH});
                    // showMessage("Requesting permission");
                }
            }else{
                Globals.IS_BLUETOOTH_PERMITTED = true;
                
                enableBluetoothAndConnect();
            }
            
        	return true;

        }else if(action.equalsIgnoreCase("send")){

            PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
            r.setKeepCallback(true);
            callbackContext.sendPluginResult(r);

            // if(Globals.IS_BLUETOOTH_PERMITTED == false){
            //     callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "BLUETOOTH_NOT_PERMITTED"));
            //     return true;
            // }

            String message = data.getString(0);

            // showMessage("sending "+message);

            if(Globals.isPeerConnected == false){

                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "HOST_UNAVAILABLE"));
                return true;
            }

            // This is the message to be sent to peer
            

            // showMessage("sending "+message);

            if(message != null && !message.isEmpty()){

                // showMessage("sending "+message+" inside");
                bluetoothUtility.write(message.getBytes());
            }

        	return true;
        }else if (action.equalsIgnoreCase("disconnect")){

            try{

                PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
                r.setKeepCallback(true);
                callbackContext.sendPluginResult(r);

                // if(Globals.IS_BLUETOOTH_PERMITTED == false){
                //     callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "BLUETOOTH_NOT_PERMITTED"));
                //     return true;
                // }

                bluetoothUtility.disconnectPeer();

                try{
                    JSONObject jObject = new JSONObject();
                    jObject.put("operation","disconnect");
                    jObject.put("status","HOST_DISCONNECTION_SUCCESSFUL");

                    r = new PluginResult(PluginResult.Status.OK, jObject);

                    callbackContext.sendPluginResult(r);

                }catch(Exception ex){
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "HOST_DISCONNECTION_FAILED"));
                }

                // callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "HOST_DISCONNECTION_SUCCESSFUL"));

            }catch(Exception ex){
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "HOST_DISCONNECTION_FAILED"));
            }

        	return true;
        }else if (action.equalsIgnoreCase("stopBroadcast")){

            try{

                PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
                r.setKeepCallback(true);
                callbackContext.sendPluginResult(r);

                if(Globals.IS_BLUETOOTH_PERMITTED == false){
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "BLUETOOTH_NOT_PERMITTED"));
                    return true;
                }

                bluetoothAdapter.setName(Globals.INITIAL_DEVICE_NAME);
                bluetoothUtility.stopBroadcast();

                Globals.shouldBroadcast = false;

                try{
                    JSONObject jObject = new JSONObject();
                    jObject.put("operation","stopBroadcast");
                    jObject.put("status","STOP_BROADCAST_SUCCESSFUL");

                    r.setKeepCallback(true);
                    r = new PluginResult(PluginResult.Status.OK, jObject);

                    callbackContext.sendPluginResult(r);

                }catch(Exception ex){

                }
                //callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "STOP_BROADCAST_SUCCESSFUL"));

            }catch(Exception ex){
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "STOP_BROADCAST_FAILED"));
            }

        	return true;
        }else if (action.equalsIgnoreCase("enableLocationService")){

            PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
            r.setKeepCallback(true);
            callbackContext.sendPluginResult(r);

            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            cordova.getActivity().startActivityForResult(intent, LOCATION_PERMISSION_REQUEST);

            return true;
        }
        
        return false;
    }




    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {

        // This sets the callback to this class
        cordova.setActivityResultCallback(this);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            cordova.getActivity().startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        }else{
            bluetoothUtility = new BluetoothUtility(cordova.getActivity());
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);

        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        webView.getContext().registerReceiver(mStateReceiver, filter);

        super.initialize(cordova,webView);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent){

        if(requestCode == Globals.VISIBILITY_CALLBACK){
            if(resultCode == 1)
                bluetoothUtility.start();
            else
                bluetoothUtility.stopBroadcast();
        }
        else if (requestCode == LOCATION_PERMISSION_REQUEST){

            if(isLocationEnabled(cordova.getActivity()) == false){

                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "BLUETOOTH_NOT_PERMITTED"));
                Globals.IS_PERMITTED = false;
            }else{
                Globals.IS_PERMITTED = true;

                PluginResult r = new PluginResult(PluginResult.Status.OK, "LOCATION_SERVICE_ENABLED");
                r.setKeepCallback(true);
                callbackContext.sendPluginResult(r);

                enableBluetoothAndConnect();
            }
        }
        else if(resultCode == -1){
            switch (requestCode){

                case REQUEST_ENABLE_BT :

//                    BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(deviceAddress);
//                    bluetoothUtility.connect(bluetoothDevice, false);
                    bluetoothUtility = new BluetoothUtility(activity);
                break;
                case REQUEST_ENABLE_BT_AND_CONNECT:

                    connectToPeer();

                    break;
                case REQUEST_ENABLE_BT_AND_BROADCAST:{
                    Globals.INITIAL_DEVICE_NAME = bluetoothAdapter.getName();

                    bluetoothUtility = new BluetoothUtility(cordova.getActivity());
                    
                    bluetoothUtility.ensureDiscoverable(Globals.deviceBroadCastName);
                    break;
                }
            }
        }
    }

    // @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,  int[] grantResults) {

        for(int r:grantResults)
        {
            if(r == PackageManager.PERMISSION_DENIED)
            {
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "PERMISSION_DENIED_ERROR"));
                Globals.IS_BLUETOOTH_PERMITTED = false;
                Globals.IS_PERMITTED = false;
                return;
            }
        }

        switch (requestCode) {
            case BLUETOOTH_PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                Globals.IS_BLUETOOTH_PERMITTED = true; 
                // if (grantResults.length > 0
                //         && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                //     // callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "PERMISSION_NOT_GRANTED"));
                //     // return true;

                    

                // } else {

                //     Globals.IS_BLUETOOTH_PERMITTED = false; 
                //     callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "PERMISSION_NOT_GRANTED"));

                // }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bluetoothUtility != null) {
            bluetoothUtility.stop();
        }
    }

    public void showMessage(final String message){

        ((Activity)cordova.getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(cordova.getActivity(), message, Toast.LENGTH_LONG).show();
            }
        });

    }

    private void connectToPeer(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(deviceAddress.toUpperCase());
                    // bluetoothUtility.connect(bluetoothDevice, false);

                    // showMessage("starting scanning");
                    bluetoothUtility.startScan() ;

                    // showMessage("scanning started");

                }catch(Exception ex){
                    // Toast.makeText(MainActivity.this, "Error: "+ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }).start();
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            
            if(locationProviders != null && locationProviders.length() > 0)
                return true;

            return false;
        }
    }

    private void enableBluetoothAndConnect(){

        Globals.IS_BLUETOOTH_PERMITTED = true;
        // String d = Globals.deviceConnectName;

        // showMessage("Name To connect: "+Globals.deviceConnectName);

        if(Globals.deviceConnectName == null){
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "HOST_NOT_PROVIDED"));
            return;
        }

        // deviceAddress = address ;

        // Globals.deviceConnectName = d;

        // PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
        // r.setKeepCallback(true);
        // callbackContext.sendPluginResult(r);

        PluginResult r = null;

        if(bluetoothAdapter.isEnabled() == false){
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            cordova.getActivity().startActivityForResult(enableIntent, REQUEST_ENABLE_BT_AND_CONNECT);
        }else{

            if(Globals.isPeerConnected == true){
                r = new PluginResult(PluginResult.Status.OK, "HOST_ALREADY_CONNECTED");
                r.setKeepCallback(true);
                callbackContext.sendPluginResult(r);

                return;

            }

            r = new PluginResult(PluginResult.Status.OK, "CONNECTING_HOST");
            r.setKeepCallback(true);
            callbackContext.sendPluginResult(r);

            connectToPeer();
        }

    }
}

